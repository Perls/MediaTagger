#!/usr/bin/perl -w


#=============================================================================
#
#    Copyright (C) 2019 Silicon Tao Technology Systems
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#=============================================================================

# Using Last.fm API
# http://ws.audioscrobbler.com/2.0/?method=track.getInfo&api_key=YOUR_API_KEY&artist=cher&track=believe&format=json
# http://ws.audioscrobbler.com/2.0/?method=artist.getinfo&artist=Cher&api_key=YOUR_API_KEY&format=json

# Considering using another API because Last.fm does not have release year as part of the album information.
# https://www.discogs.com/developers

# Known issues.
# JSON::API does not work with JSON:XS 4.x
# https://github.com/geofffranks/json-api/issues/4
# I removed the CPAN installed JSON::XS and installed the package from the repo. Now it works.

# Example of use.
# List albums by given artist
# ./GetTrackInfo.pl -dj -albums --artist="Terri Clark" |jq ".[]|.album[].name"

# perl -MCPAN -e "install JSON::API"
use JSON::API;
use JSON::MaybeXS qw(encode_json decode_json);

# To use the modules in the local directory
use FindBin qw($RealBin);
use lib "$RealBin";
use StConfig;
use MtCommon;


my $api;
my $obj;
my %AppVars;
my $ArtistMbid;
my $ArtistName;
my $AlbumMbid;
my $AlbumArt;
my $AlbumName;

%Op = ( 
	# Arguments that start with double hyphen (--) require a value.
	# i.g.) --a "Stevie Ray Vaughan"
	"-dj" => "DUMP_JSON", 	# Show API query results in JSON format.
	"--artist" => "ARTIST",	# Lookup using this artist name. By itself it will return just information about the artist.
	"-albums" => "ALBUMS",	# List albums from given artist
	"--album" => "ALBUM",	# Lookup using this album name.
	"--track" => "TRACK",	# Lookup using this track name.
	"-d" => "DEBUG",		# Print a lot of junk to the screen.
	"-v" => "VERBOSE",		# Print messages about what is happening.
	"-h" => "HELP",			# Show help.
);


sub Main {
	ReadArgs();
	ShowHelp() if $Options{HELP};
	StConfig::ReadConfig(\%AppVars);

	$api = JSON::API->new("http://ws.audioscrobbler.com/2.0/",
		'debug' => $Options{DEBUG}
		);

	GetArtist() if ($Options{ARTIST});
	GetAlbums() if ($Options{ALBUMS} && $ArtistName);
	GetAlbum() if ($Options{ALBUM} && $ArtistName);
	DumpJson($obj) if ($Options{DUMP_JSON});
	DumpSummary();
}

sub DumpSummary {
	print "ArtistName: $ArtistName\n";
	print "AlbumName: $AlbumName\n";
	print "AlbumArt: $AlbumArt\n";
}

sub DumpJson {
	my $Obj = shift;

	# This will print the JSON so it can be pipped to jq
	# ./GetTrackInfo.pl --artist="Metallica" --dj | jq ".[]"
	my $JSobj = JSON::MaybeXS->new(utf8 => 1, pretty => 1); #, sort_by => 1);
	print $JSobj->encode($Obj);
	exit(0);
}

sub GetAlbums {
	if ($obj = $api->get("/", {
				'method' => 'artist.gettopalbums', 
				'artist' => $ArtistName,
				'api_key' => $AppVars{"API_KEY"},
				'format' => 'json' } )) {
		# 		
	} else {
		print "error ".$api->errstr . "\n";
	}		
}

sub GetAlbum {	
	if ($obj = $api->get("/", {
				'method' => 'album.getinfo', 
				'album' => $Options{ALBUM},
				'artist' => $ArtistName,
				'api_key' => $AppVars{"API_KEY"},
				'format' => 'json' } )) {
		
		# The $obj has been decoded to Perl and is returned as
		# a reference to a hash, that contains nested hashs.
		$AlbumMbid = $obj->{"album"}{"mbid"};
		$AlbumName = $obj->{"album"}{"name"};

		print "Album Name: $AlbumName\n" if $Options{VERBOSE};
		print "Album MBID: $AlbumMbid\n" if $Options{VERBOSE};
		foreach my $Img (@{$obj->{"album"}{"image"}}) {
			print @{$Img}{"size"}." @ ".@{$Img}{"#text"}."\n" if $Options{VERBOSE};			
			$AlbumArt = @{$Img}{"#text"} if @{$Img}{"#text"} =~ m/^http/;
		}
	} else {
		print "error ".$api->errstr . "\n";
	}		
}

sub GetArtist {	
	if ($obj = $api->get("/", {
				'method' => 'artist.getinfo', 
				'artist' => $Options{ARTIST},
				'api_key' => $AppVars{"API_KEY"},
				'format' => 'json' } )) {

		# The $obj has been decoded to Perl and is returned as
		# a reference to a hash, that contains nested hashs.
		$ArtistMbid = $obj->{"artist"}{"mbid"};
		$ArtistName = $obj->{"artist"}{"name"};

		print "Artist Name: $ArtistName\n" if $Options{VERBOSE};
		print "Artist MBID: $ArtistMbid\n" if $Options{VERBOSE};
	} else {
		print "error ".$api->errstr . "\n";
	}		
}


Main();
