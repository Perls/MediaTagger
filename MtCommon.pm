package MtCommon;


#=============================================================================
#
#    Copyright (C) 2019 Silicon Tao Technology Systems
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#=============================================================================

use File::Basename;
use parent 'Exporter';
our @Recomendations;
our @WorkPaths;
our %Options;
our %Op;

our @EXPORT = qw(@Recomendations @WorkPaths %Options %Op
	Recomend PrintRecomendations ReadArgs ShowHelp LocateRootFolder );

sub Recomend {
	$Message = shift;
	push(@Recomendations, $Message) if (! grep(/\Q$Message/, @Recomendations) );
}

sub PrintRecomendations {
	foreach $Message (@Recomendations) {
		print "Recomending: $Message\n";
	}
}

sub ReadArgs() {
	foreach $Arg (@ARGV) {
		if(exists $Op{$Arg}) {
			$Options{$Op{$Arg}} = 1;
			next;
		} 
		if($Arg =~ /^--.*=/) {
			my ($An, $Av) = split(/=/, $Arg);
			if(exists $Op{$An}) {
				$Options{$Op{$An}} = $Av;
				next;
			}
		}
		if ( -d $Arg ) {
			push(@WorkPaths, $Arg);
			next;
		}
		if ( -f $Arg ) {
			push(@WorkPaths, $Arg);
			next;
		}
		die "Unknow command or path $Arg";
	}
}

sub ShowHelp {
	my $AppName = $0;
	print basename($AppName)."\n";
	open IN, "<", $AppName;
	while (<IN>) {
		my $L = $_;
		$L =~ s/"//g;
		#$L =~ s/'//g;
		$L =~ s/;//g;
		print $L if (/^\%Op =/../^\)\;/);
	}
	close IN;
	exit(1);
}

sub LocateRootFolder {
	# This value overrides the discovery. 
	return $Options{TARGET_DIR} if $Options{TARGET_DIR};

	# This will attpemt to identify the commnon directory that the music is saved in.
	my $PathFile = shift;
	my $OrigPath = $PathFile;
	my $ReturnPath = "";
	my $Sub2 = basename($PathFile);
	my $Sub1 = $Sub2;
 	while($PathFile =~ /[a-z]/i) {
		# print "PathFile = '$PathFile'\n" if $Options{DEBUG};
		$PathFile = dirname($PathFile);
		my $Fail = 0;
		for my $I (A..Z) { 
			# print "does '$PathFile/$I' exist\n" if $Options{DEBUG};
			if(! -d "$PathFile/$I") {
				$Fail++;
			}
			if($Fail > 4) {
				next;
			}
		}
		# sleep 1 if $Options{DEBUG};
		# print "Sub1 = '$Sub1'\nSub2 = '$Sub2'\nFail = $Fail\n" if $Options{DEBUG};
		if($Fail < 5) {
			my $Sp = "$PathFile/$Sub1/$Sub2";
			# printf "Sp = '$Sp'\n" if $Options{DEBUG};
			if ($OrigPath =~ m/\Q$Sp/) {
				$ReturnPath = $Sp;
			}
			# print "RootFolder = $ReturnPath\n" if $Options{DEBUG};
			return $ReturnPath;
		}
		$Sub2 = $Sub1;
		$Sub1 = basename($PathFile);
	}
	return "";
}

1;
