# MediaTagger.pl

This script will search a given directory for MP3 files. It will try to determin the artis, year, album and cover art.
Options can be given to write these values to the ID3 tags of the file.

These scripts are written to work with a specific directory structure for sorting music.

```
<MUSIC DIR>/<FIRST LETTER OF ARTIST>/<ARTIST>/<YEAR> - <ALBUM>/<FILENAME>
```
This is refered to as AYA (Artist/Year - Album) directory.

To determin the artist name if ID3 tag information is missing, the __<MUSIC DIR>__ must contain a sub directory for each letter of the alphabet.

## Making Changes To Files

No ID3 tags are overwritten by default. 

If any values of Artist, Year or Album are know, these values will be saved to the file __Album.nfo__ in the AYA directory.

If cover art is set and there is no local file for {Cover,Folder}.{jpg,jpeg,png} the the image data is saved to Folder.jpg.

If cover art is not set and a local file of {Cover,Folder}.{jpg,jpeg,png} exists, it will be set as the album art when the -c option is used.

Read the help for command options. __./MediaTagger.pl -h__

## Stripping Leading The
The word "The " is removed from the leading part of the artist name.

For example "The Guess Who" album "American Woman" from 1970 has a path of:
__My Music/G/Guess Who/1970 - American Woman__

## Music Data Priority

This is the order or priority for AYA data.
1. If the music files are in a directory of AYA format, these fields for artist, year and album are authoritive.
2. If not in an AYA directory format, ID3 tags are authoritive and tag values can be saved to a file Album.nfo in the directory.
3. If ID3 tag fields are missing values, values can be read from the Album.nfo file. These values can be written to ID3 Tag using the __-b__ option.
4. If given the argument of --aya, the values for Artist, Year and Album are taken from the argument. This overrides all other sources and writes to __Album.nfo__
5. If given the argument of --a, the value for Artist is taken from the argument. This overrides all other sources including __--aya__

## Other Tools

### SmartMove.pl

If the music directory is not currently in AYA format, SmartMove.pl can be used to move files to 
their AYA location by reading the artist, year and album from ID3 tags.

If Year or Album are missing the files can be moved to the Artist directory.

See help for command options. __./SmartMove.pl -h__

### GetTrackInfo.pl 

This script can be used to locate a downloadable image for album cover art.

This script uses [Last.fm API](https://www.last.fm/api/) to get information about Artist, Album and Track.

To use this, an API key is required and must be in a config file in the users home directory.

__~/.GetTrackInfo.conf__
```
API_KEY=<YOUR KEY>
```

Example of the API commands using curl. The tool [jq](https://stedolan.github.io/jq/) is a great tool to format and query the JSON data.
```
source ~/.GetTrackInfo.conf
curl http://ws.audioscrobbler.com/2.0/\?method\=track.getInfo\&api_key\=${API_KEY}\&artist\=Golden%20Earring\&track\=Twilight%20Zone\&format\=json | jq ".[]"
curl http://ws.audioscrobbler.com/2.0/\?method\=album.getInfo\&api_key\=${API_KEY}\&mbid\=368e0aa2-3fef-494e-b080-4fbd62736a39\&format\=json | jq ".[]"
```

See help for command options. __./GetTrackInfo.pl -h__

### Scott2Mp3.pl

This is a tool that was created in 2003 to convert Scott files to MP3. Scott files are MP3's with a binary header that identifies the file to a Scott player that is commonly used by radio stations.

This file is no longer supported.

### Daf3Mpx.pl

This is a tool that was created in 2003 to convert DAF files to MP3.

This file is no longer supported.


