#!/usr/bin/perl -w


#=============================================================================
#
#    Copyright (C) 2019 Silicon Tao Technology Systems
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#=============================================================================

# sudo apt install libmp3-tag-perl libimage-size-perl libimage-magick-perl

use MP3::Tag;
use File::Find;
use File::Basename;
use File::Copy;

# To use the modules in the local directory
use FindBin qw($RealBin);
use lib "$RealBin";
use MtCommon;

%Op = ( 
	"--root" => "TARGET_DIR", # Set the target root directory for destination. 
	"-m" => "MOVE", 	# Move the files to AYA path, default is to recomend move.
	"-d" => "DEBUG", 	# Show debug output.
	"-v" => "VERBOSE",  # Verbosly describe what is happening.
	"-h" => "HELP", 	# Show help.
);

sub Main() {
	ReadArgs();
	ShowHelp() if $Options{HELP};
	foreach $Path (@WorkPaths) {
		if(-f $Path) { 
			Process($Path); 
			next;
		}
		if(-d $Path) {
			find(\&wanted, $Path);
		}
	}
	PrintRecomendations();
}

sub OrigLocateRootFolder {
	# This will attpemt to identify the commnon directory that the music is saved in.
	my $PathFile = shift;
	my $OrigPath = $PathFile;
	my $ReturnPath = "";
	while($PathFile =~ /[a-z]/i) {
		$PathFile = dirname($PathFile);
		my $Fail = 0;
		for my $I (A..Z) { 
			if(! -d "$PathFile/$I") {
				$Fail++;
			}
		}
		if($Fail < 5) {
			for my $I (A..Z) { 
				my $Sp = "$PathFile";
				if ($OrigPath =~ m/\Q$Sp/) {
					$ReturnPath = $Sp;
				}
			}
			return $ReturnPath;
		}
	}
	return "";
}

sub wanted {
	/.*mp3$/ && Process($File::Find::name);
}

sub Process {
	$File = shift;
	my $Dn = dirname($File);
	my $Bn = basename($File);
	my $Mp3 = MP3::Tag->new($File);
	print "Process '$File'\n" if $Options{DEBUG};
	$Mp3->get_tags;
	my $MusicRoot = LocateRootFolder($File);
	my ($title, $track, $artist, $album, $comment, $year, $genre) = $Mp3->autoinfo();

	
	if($MusicRoot =~ /[a-z]/i) {
		print "MusicRoot '$MusicRoot'\n" if $Options{DEBUG};
		my $Target = "";
		if($artist =~ /[a-z]/i) {
			print "artist '$artist'\n" if $Options{DEBUG};
			$Target = "$MusicRoot/".uc(substr($artist,0,1))."/$artist";
			if($year eq "") { $year = 1900; }
			if(($year > 1900) && ($album =~ /[a-z]/i)) {
				$Target = "$MusicRoot/".uc(substr($artist,0,1))."/$artist/$year - $album";
			}
		}
		if($Target =~ /[a-z]/i) {
			print "Target '$Target'\n" if $Options{DEBUG};
			if(! -f "$Target/$Bn") {
				MakeDir($Target);				
				if($Options{MOVE}) {
					move("$File", "$Target/$Bn");
					print "mv \"$File\" \"$Target/$Bn\"\n" if $Options{VERBOSE};
				} else {
					&Recomend("move \"$File\" \"$Target/$Bn\"");
				}
			} else {
				&Recomend("Manual move \"$File\" \"$Target/$Bn\"");
			}
		}
	}
	$Mp3->close();	
}

sub MakeDir {
	my $Td = shift;
	my @Dirs = split /\//, $Td;
	my $Bd = "";
	foreach my $D (@Dirs) {
		$Bd = $Bd . $D . "/";
		if(! -d $Bd) {
			print "mkdir $Bd, 0755\n" if $Options{VERBOSE};
			mkdir $Bd, 0755 if $Options{MOVE};
		}
	}

}

Main;
