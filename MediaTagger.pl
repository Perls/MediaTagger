#!/usr/bin/perl -w


#=============================================================================
#
#    Copyright (C) 2019 Silicon Tao Technology Systems
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#=============================================================================

=pod

=head1 MediaTagger

This script will search a given directory for MP3 files. It will try to determin the artis, year, album and cover art.
Options can be given to write these values to the ID3 tags of the file.

These scripts are written to work with a specific directory structure for sorting music.

<MUSIC DIR>/<FIRST LETTER OF ARTIST>/<ARTIST>/<YEAR> - <ALBUM>/<FILENAME>

To determin the artist name if ID3 tag information is missing, the <MUSIC DIRECTORY> must contain a sub directory for each letter of the alphabet.

=head2 Making Changes To Files

Album cover art is not overwritten by default. 

If any values of Artist, Year or Album are know, these values will be saved to Album.nfo.

If cover art is set and there is no local file for {Cover,Folder}.{jpg,jpeg,png} the the image data is saved to Folder.jpg

IF cover art is not set and a local file of {Cover,Folder}.{jpg,jpeg,png} exists, it will be set as the album art when the -c option is used.

Read the help for command options. I<./MediaTagger.pl -h>

=head2 The word "The " is removed from the leading part of the artist name.
For example "The Guess Who" album "American Woman" from 1970 has a path of:
  My Music/G/Guess Who/1970 - American Woman

=head2 Music Data Priority

=over

=item - 

If the music files are in a directory of this format, these fields for artist, year and album are authoritive.

=item -

If not in this directory format, ID3 tags are authoritive and tag values can be saved to a file Album.nfo in the directory.

=item - 

If ID3 tag fields are missing values, values can be read from the Album.nfo file.

=item - 

If given the argument of --aya, the values for Artist, Year and Album are taken from the argument and overrides all other sources.

=item - 

If given the argument of --a, the value for Artist is taken from the argument and overrides all other sources.

=back

=head2 Other Tools

=head3 SmartMove.pl

If the music directory is not currently in AYA format, SmartMove.pl can be used to move files to 
their AYA location by reading the artist, year and album from ID3 tags.

If Year or Album are missing the files can be moved to the Artist directory.

See help for command options. I<./SmartMove.pl -h>

=head3 GetTrackInfo.pl 

This script can be used to locate a downloadable image for album cover art.

See help for command options. I<./GetTrackInfo.pl -h>

=cut


#######################################################################
# Installing needed Perl libraries
# sudo apt install libmp3-tag-perl libimage-size-perl libimage-magick-perl \
#					libfile-homedir-perl libjson-xs-perl jq
#
# Installing with CPAN for libraries not available from package vendor.
# Script to search for modules
# echo "i /DBIx/" | sudo perl -MCPAN -e shell </dev/stdin
# echo "i /nosql/" | sudo perl -MCPAN -e shell </dev/stdin | grep -i "DBIx::NoSQL[[:blank:]]"
# sudo perl -MCPAN -e "install DBIx::NoSQL"


# Helpful links
# https://sveinbjorn.org/get-album-artwork-id3-using-perl
# https://krkeegan.com/perl-id3tag-and-errors-with-cover-art/
# http://www.drdobbs.com/web-development/managing-your-mp3-library-in-perl/184416027
# https://forums.slimdevices.com/showthread.php?16541-Use-Perl-to-add-Cover-Art-to-mp3

# Bug in MP3::Tag
# https://rt.cpan.org/Public/Bug/Display.html?id=105165


use MP3::Tag;
use File::Find;
use File::Basename;
use Image::Size;
use Image::Magick;
use File::Copy;

# To use the modules in the local directory
use FindBin qw($RealBin);
use lib "$RealBin";
use MtCommon;

%Op = ( 
	# Arguments that start with double hyphen (--) require a value.
	# i.g.) --a='Stevie Ray Vaughan'
	"--a" => "ARTIST",	# Overwrite the NFO file and ID3 Tags with given artist.
	"--aya" => "AYA", 	# Overwrite the NFO file and ID3 Tags with given <ARTIST/YYYY - ALBUM>.
	"-r" => "REPORT", 	# Show information about MP3 file.
	"-c" => "COVER", 	# Update the cover art from local file.
	"-l" => "LIST", 	# List tracks that need work.
	"-b" => "REBUILD", 	# Read the ID3 tag, delete the tag, create new tag, set known values. Update cover art.
	"-d" => "DEBUG", 	# Show debug output.
	"-dd" => "DEBUG2",	# Debug problem areas
	"-v" => "VERBOSE",  # Verbosly describe what is happening.
	"-h" => "HELP", 	# Show help.
);

sub Main() {
	ReadArgs();
	ShowHelp() if $Options{HELP};
	foreach $Path (@WorkPaths) {
		if(-f $Path) { 
			Process($Path); 
			next;
		}
		if(-d $Path) {
			print "Finding in path = $Path\n" if $Options{DEBUG};
			find(\&wanted, $Path);
		}
	}
	PrintRecomendations();
}

sub wanted {
	/.*mp3$/i && Process($File::Find::name);
}

sub Process {
	$File = shift;
	printf("open $File and read tags\n") if $Options{DEBUG};
	my $Mp3 = MP3::Tag->new("$File");	

	# V2.4 is not fully supported by MP3::Tag but for writing artist, album, year and cover art it seems to work.
	$Mp3->config(write_v24 => 1);

	$Mp3->get_tags;
	# my ($Id3title, $Id3track, $Id3artist, $Id3album, $Id3comment, $Id3year, $Id3genre) = $Mp3->autoinfo();
	# my ($title, $track, $artist, $album, $comment, $year, $genre) = $Mp3->autoinfo();
	my $Id3Hash = $Mp3->autoinfo();
    my $height = 0;
    my $width = 0;
	my $Cover = "blank";
	my $Artist = "";
	my $Year = 1900;
	my $Album = "";
	my $Title = "";
	my $Track = "";
	my $Genre = "";
	my $ArtistDir = LocateRootFolder($File);
	my $AlbumDir = dirname($File);

	# If the music files are in a sub folder of the album folder 
	# i.e.) ./<ARTIST>/<YEAR> - <ALBUM>/CD1/*mp3, ./<ARTIST>/<YEAR> - <ALBUM>/Disk1/*mp3
	printf "AlbumDir = '$AlbumDir'\nArtistDir = '$ArtistDir'\n" if $Options{DEBUG2};
	my $ReduceAlbumDir = $AlbumDir;
	if(($ArtistDir) && ($ReduceAlbumDir =~ m|^$ArtistDir|)) {
		while (dirname($ReduceAlbumDir) ne $ArtistDir) {
			$ReduceAlbumDir = dirname($ReduceAlbumDir);		
			printf "ReduceAlbumDir = '$ReduceAlbumDir'\nArtistDir = '$ArtistDir'\n" if $Options{DEBUG2};
			sleep 5  if $Options{DEBUG2};
			$ReduceAlbumDir =~ m|^$ArtistDir| or last;
		}
	}
	printf "ReduceAlbumDir = '$ReduceAlbumDir'\nArtistDir = '$ArtistDir'\n" if $Options{DEBUG2};
	$AlbumDir = $ReduceAlbumDir if $ReduceAlbumDir =~ m|^$ArtistDir|;

	# Artist/Year - Album from path
	my $ParseAYA;
	# This conditional opperation is not working as advertised.
	# defined $Options{AYA} ? $ParseAYA = $Options{AYA} : $ParseAYA = basename($ArtistDir)."/".basename($AlbumDir);
	if(defined $Options{AYA}) {
		$ParseAYA = $Options{AYA};
	} else {
		$ParseAYA = basename($ArtistDir)."/".basename($AlbumDir);
	}
	printf ("AlbumDir = '%s'\nOptions{AYA} = '%s'\nParseAYA = '%s'\n", 
		$AlbumDir, 
		(($Options{AYA}) ? $Options{AYA} : ""),
		(($ParseAYA) ? $ParseAYA : "")) if $Options{DEBUG};
	
	if($AlbumDir =~ /[a-z]/i) {
		my $P1 = $ParseAYA;
		$P1 =~ s/^\///;
		# print "P1 = '$P1'\n";
		if($P1 =~ m/^(?<artist>.*)\/(?<year>\d{4}) - (?<album>.*)/) {
			$Artist = $+{artist} if($+{artist});
			$Year = $+{year} if($+{year});
			$Album = $+{album} if($+{album});
		}
		$Artist = $Options{ARTIST} if $Options{ARTIST};
		if ($Options{DEBUG}) {
			printf "\n------------ Path Values ------------\n";
			printf "ArtistDir = $ArtistDir\n";
			printf "AlbumDir = $AlbumDir\n";
			printf "File: $File\n";
			printf "Artist = $Artist\n";
			printf "Year = $Year\n";
			printf "Album = $Album\n";
		}
	} else {
		print "Failed to locate ArtistDir\n";
		exit(__LINE__);
	}

	# printf "Line %d Artist = '%s'\n", __LINE__, $Artist;

	# If the path does not set the "Artist/Year - Album", take what the tag offers.
	if($Artist eq "") {
		$Artist = $Id3Hash->{"artist"};
	}
	$Year = 1900 if $Year eq "";
	if(($Year < "1901") && (defined $Id3Hash->{"year"})) {
		$Year = $Id3Hash->{"year"};
		$Year = 1900 if $Year eq "";
	}
	if($Album eq "") {
		$Album = $Id3Hash->{"album"};
	}		
	$Title = $Id3Hash->{"title"}; 
	if($Title eq "") {
		$Title = $Id3Hash->{"song"}; 
	}
	$Genre = $Id3Hash->{"genre"};

	# If the tag does not have this info, read from NFO file.
	if((-d $AlbumDir) && ($ArtistDir ne $AlbumDir)) {
		$Artist = &ReadAlbumInfo($AlbumDir, "Artist") if(! $Artist);
		$Album = &ReadAlbumInfo($AlbumDir, "Album") if(! $Album);
		$Year = &ReadAlbumInfo($AlbumDir, "Year") if ($Year == 1900);
		$Year = 1900 if $Year eq "";
	}

	my $imgdata;
	if(my $id3v2_tagdata   = $Mp3->{ID3v2} ) {
    	my $info            = $id3v2_tagdata->get_frame("APIC");
    	$imgdata         = $$info{'_Data'};
	    if($imgdata) {
	    	($height, $width ) = imgsize(\$imgdata);	    		
	    }
	    $mimetype        = $$info{'MIME type'};
	}
	&Recomend("Organizing this folder '$AlbumDir'") if($Year == 1900);
	
	$Cover = &GetCovers($AlbumDir);
	if( (($height) ? $height : 0) + (($width) ? $width : 0) < 100) {		
		if(($Options{'LIST'}) && (-f $Cover)) {
			&Recomend("Update cover '$Cover' for '$File'");
		} else {			
			&Recomend("Recomend downloading a cover for '$AlbumDir'") if (! -f $Cover);
		}
	}

	print "Cover is '$Cover'\n" if $Options{DEBUG};
	if((! -f $Cover) && (($height + $width) > 100)) {
		my ($m1, $m2) = split(/\//, $mimetype);
		if((defined $m2) && ($m2 =~ /jpeg/i)) {
			$m2 = "jpg";
		}
		my $dest;
		if($m2) {
			$dest = $AlbumDir."/Folder.".$m2;
		} else {
			&Recomend("Fix image type in ID3 Tag for $File");
		}
		print "Saving $dest from ID3v2 tag\n" if $Options{DEBUG};
 		open(ARTWORK, ">$dest") or return("Error writing $dest");
        binmode(ARTWORK);
        print ARTWORK $imgdata;
        close(ARTWORK);
	}
	
	if($Options{REBUILD}) {
		# Keep:
		#$Id3title, $Id3track, $Id3artist, $Id3album
		#print "What is in Id3Hash\n";
		#foreach my $Key (keys %$Id3Hash) {
		#	print "\$Id3Hash->{$Key} = '$Id3Hash->{$Key}'\n";
		#}
		$Mp3->{ID3v2}->remove_tag if exists $Mp3->{ID3v2};
		$Mp3->{ID3v1}->remove_tag if exists $Mp3->{ID3v1};
		$Mp3->new_tag("ID3v2");
		$Mp3->{ID3v2}->write_tag;

		if ($Options{DEBUG}) {
			printf "\n------------ Rebuild Values ------------\n";
			printf "File = $File\n";
			printf "Artist = $Artist\n";
			printf "Year = $Year\n";
			printf "Album = $Album\n";
			printf "Track = $Track\n";
			printf "Title = $Title\n";
			printf "Genre = $Genre\n";
		}
		$Options{COVER} = 1;
		$Mp3->update_tags({year => $Year}) if ($Year > 1900);
		$Mp3->update_tags({album => $Album}) if $Album;
		$Mp3->update_tags({artist => $Artist}) if $Artist;
		$Mp3->update_tags({title => $Title}) if $Title;
		$Mp3->update_tags({track => $Track}) if $Track;
		$Mp3->update_tags({genre => $Genre}) if $Genre;
	}	
	if($Options{COVER}) {
		printf "Cover = '$Cover'\n";
		if(-f $Cover) {
			printf "\n------------ Set Cover Art ------------\n";
			printf("File: %s\n", $File);
			printf("Setting cover art to '%s'\n", $Cover);
			# Copyright 2004, Marcus Thiesen (marcus@thiesen.org)
			use constant APIC => "APIC";
			use constant TYPE => "jpg";
			use constant HEADER => ( chr(0x0) , "image/" . TYPE , chr(0x3), "Cover Image");
			my $image = new Image::Magick;
			my $x = $image->Read($Cover);
			my $imagedata = $image->ImageToBlob(magick => TYPE );
			my $id3;
			if (exists $Mp3->{ID3v2}) {
				$id3 = $Mp3->{ID3v2};
			} else {
				$id3 = $Mp3->new_tag("ID3v2");
			}

			my $frames = $id3->supported_frames();
			if (!exists $frames->{APIC}) {
				print "Something is wrong, APIC is not a supported frame!\n";
				exit 2;
			}

			my $frameids = $id3->get_frame_ids();
			if (exists $$frameids{APIC}) {
				print "Adding imagedata to tag\n" if $Options{DEBUG};
				$id3->change_frame(APIC, HEADER, $imagedata);
			} else {
				print "Create new tag with imagedata\n" if $Options{DEBUG};
				$id3->add_frame(APIC,HEADER, $imagedata);
			}

			$id3->write_tag();
		}
	}
	if($Options{'REPORT'}) {		
		printf "\n------------ Report ------------\n";
		printf("File: %s\n", $File);
		printf("Title: %s\n", ($Title) ? $Title : "");
		printf("Track: %s\n", ($Track) ? $Track : "");
		printf("Artist: %s\n", ($Artist) ? $Artist : "");
		printf("Album: %s\n", ($Album) ? $Album : " ");
		printf("Year: %s\n", ($Year) ? $Year : $Year);
		printf("Artist Dir: %s\n", $ArtistDir);
		printf("Album Dir: %s\n", $AlbumDir);
		if( ($height + $width) > 100) {
			printf("Cover art: %dx%d, type %s\n", $height, $width, $mimetype);
		} else {
			printf("Cover art: ID3 tag is blank");			
			if(-f $Cover) {
				printf(" but an acceptable image file was found. Use '-c' to update the cover image.");
				&Recomend("Setting cover art for '$AlbumDir'");
			}
			printf("\n");
		}
	}
	$Mp3->close();	
	print "AlbumDir = '$AlbumDir'\n" if $Options{DEBUG};
	if((-d $AlbumDir) && ($ArtistDir ne $AlbumDir)) {
		&SaveAlbumInfo($AlbumDir, "Artist", $Artist) if $Artist;
		&SaveAlbumInfo($AlbumDir, "Album", $Album) if $Album;
		&SaveAlbumInfo($AlbumDir, "Year", $Year) if ($Year > 1901);
	}
}

sub GetCovers {
	my $Dir = shift;
	my @DirList;
	my $Return = "No cover";
	my $Failed = 0;
	my $RecomendFix = "";
	opendir my $dh, $Dir || die "Can't open directory $Dir\n";
	while(my $Entry = readdir $dh) {
		push(@DirList, $Entry);
	} 
	closedir $dh;
	my @Cnames = ("cover.jpg", "folder.jpg", "front.jpg", "cover.png", "folder.png", "front.png");
	foreach my $Cn (@Cnames) {		
		# The return from grep must be in parenthesis to get the name of the entry, or else it just returns status code.
		if((my $Test) = grep (/$Cn$/i, @DirList)) {
			$Return = "$Dir/$Test";
			$RecomendFix = "";
			printf "Check image file '$Dir/$Return'\n" if $Options{DEBUG};
			$Failed = 0;
			my ($ImageX, $ImageY) = imgsize($Return);
			printf("%s %dx%d\n", $Return, $ImageX, $ImageY) if $Options{DEBUG};
			my $Diff = $ImageX - $ImageY;
			if($Diff < 0) { $Diff = $ImageY - $ImageX; }
			if($Diff > (($ImageY + $ImageX) /20) ) { 
				$Failed += 1; 
				$RecomendFix = "ratio";
				printf "Image file bad ratio '$Dir/$Return'\n" if $Options{DEBUG};				
			}
			if(($ImageY + $ImageX) > 1000) { 
				$Failed += 1; 
				$RecomendFix = "size";
				printf "Image file too large '$Dir/$Return'\n" if $Options{DEBUG};				
			}
			if(($ImageY + $ImageX) < 100) { 
				$Failed += 1; 
				$RecomendFix = "by downloading";
				printf "Image file too small '$Dir/$Return'\n" if $Options{DEBUG};
			}
			# print "Fail count = $Failed\n"; # Really should not use that image.
			if($Failed == 0) { 
				printf "Image file acceptable '$Dir/$Return'\n" if $Options{DEBUG};
				last;
			}
		}		
	}
	# Should check if the image is the right size and shape
	&Recomend("Fixing image $RecomendFix '$Dir/$Return'") if ($Failed > 0);
	printf "Returning image file '$Dir/$Return'\n" if $Options{DEBUG};

	return $Return;
}

sub ReadAlbumInfo {
	my $AlbumRoot = shift;
	my $FieldName = shift;
	my $Return = "";

	Touch("$AlbumRoot/Album.nfo") if(! -f "$AlbumRoot/Album.nfo");

	open IN, "<", "$AlbumRoot/Album.nfo" or die "Can't read nfo file '$AlbumRoot/Album.nfo'\n";
	while (my $Line = <IN>) {
		chomp $Line;
		my ($Var, $Val) = split /: /, $Line;
		$Return = $Val if $Var eq $FieldName;
	}
	close IN;
	return($Return);
}

sub SaveAlbumInfo {
	my $AlbumRoot = shift;
	my $FieldName = shift;
	my $NewValue = shift;
	my $FieldLocated = 0;

	# print "Write config value '$FieldName: $NewValue' to '$AlbumRoot/Album.nfo'\n" if $Options{DEBUG};
	Touch("$AlbumRoot/Album.nfo") if (! -f "$AlbumRoot/Album.nfo");
	move("$AlbumRoot/Album.nfo", "$AlbumRoot/Album.nfo_orig") or die "Can't change $AlbumRoot/Album.nfo";
	open OUT, ">>", "$AlbumRoot/Album.nfo" or die "Can't create file '$AlbumRoot/Album.nfo'";
	open IN, "$AlbumRoot/Album.nfo_orig";
	while (my $Line = <IN>) {
		chomp $Line;
		my ($Var, $Val) = split /: /, $Line;
		if ($Var eq $FieldName) {
			printf OUT "$FieldName: $NewValue\n";
			$FieldLocated = 1;
		} else {			
			printf OUT "$Line\n";
		}
	}
	close IN;
	if($FieldLocated == 0) {
		print "Append with '$FieldName: $NewValue'\n" if $Options{DEBUG};
		printf OUT "$FieldName: $NewValue\n";
	}
	close OUT;
	unlink "$AlbumRoot/Album.nfo_orig" if (-f "$AlbumRoot/Album.nfo_orig");
}

sub Touch {
	my $FileToTouch = shift;
	open OUT, ">>", $FileToTouch or die "Can't create file '$FileToTouch'";
	close OUT;	
	print "Created new INFO file '$FileToTouch'\n" if $Options{VERBOSE};
}

Main;
