#!/usr/bin/perl

#=============================================================================
#
#    Copyright (C) 2003-2019 Silicon Tao Technology Systems
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#=============================================================================

use FileHandle;
use File::Copy;

# Global vars
$SourceDir = "";
$DestDir = "";
$DeleteOldFile = 0;

&main();
exit;

sub ReadArguments
{
   local ($Argument);
   local ($Position);
   
	if(($#ARGV == 1)||($#ARGV == 2))
	{
		$SourceDir = shift @ARGV;
		$DestDir = shift @ARGV;
		if(! -d $DestDir)
		{
			mkdir($DestDir);
		}
		$TestForDelete = shift @ARGV;
		&ColorPrinting("yellow");
		print "=============================\n";
		if($TestForDelete eq "--delete_daf_files")
		{
			&ColorPrinting("red");
			print "Set delete DAF files ON\n";
			&ColorPrinting("yellow");
			print "10 second pause, press CTRL-C to exit\n";
			system("sleep 10");
			&ColorPrinting("off");
			$DeleteOldFile = 1;
		} else
		{
			print "Keeping DAF files, delete option is OFF\n";
		}
		&ColorPrinting("off");
	} else
	{
		&ShowHelpInfo();
		exit(1);
	}
}

sub GetEnvVar
{
   local ($VarName) = @_;
   local ($TempReturn);
   
   #print "Posted = ".$ENV{'QUERY_STRING'};
   foreach $key (keys %ENV)
   {
      if($key eq $VarName)
      {
         $TempReturn = $ENV{$key};
      }
      #print "$key = $ENV{$key}\n";
   }
   return($TempReturn);
}

sub ShowHelpInfo
{
   print "Daf2Mpx.pl from www.SiliconTao.com\tGNU GPL licensed!\n\n";
   print "This program converts DAF audio files into MP3 format. DAF files are music\n";
   print "files in MP3 format with code file names and .daf file extentions.\n\n";
   print "This program will attempt to copy them from your source dir to your\n";
   print "destination dir and then rename them there.\n";
   print "Your two settings for where these directories are given as arguments.\n";
   print "Example:\nDaf2Mpx.pl /home/tom/daf/ /home/tom/mp3/\n";
	print "Optionaly you can give a third argument to delete the DAF files after they are converted.\n";
	print "Daf2Mpx.pl /home/tom/daf/ /home/tom/mp3/ --delete_daf_files\n";
}

sub ConvertThisFile
{
   local ($FileToChange) = @_;
   
   $NewFileName = "";
   $TheDafInfo = &ReadDafId($FileToChange);
	
   if($TheDafInfo gt "")
   {
      $NewFileName = $DestDir."/".$TheDafInfo.".mp3";
		if((! -f $NewFileName)&&(-f $FileToChange))
		{
			#print "Yes do that please\n";
			copy($FileToChange,$NewFileName);			
			&ColorPrinting("green");
			print "Converted $FileToChange to $NewFileName\n";
			system("echo \"Converted $FileToChange to $NewFileName\" >> Daf2Mpx.convertlog");
			&ColorPrinting("off");
			if((-f $NewFileName)&&(-f $FileToChange))
			{
				if($DeleteOldFile == 1)
				{
					&ColorPrinting("yellow");
					print "Deleting $FileToChange\n";
					system("echo \"Deleting $FileToChange\" >> Daf2Mpx.convertlog");
					unlink($FileToChange);
					&ColorPrinting("off");
				}
			}
		} else
		{
			if(-f $NewFileName)
			{
				&ColorPrinting("red");
				print "Counld not convert $FileToChange because $NewFileName already exists.\n";
				system("echo \"Counld not convert $FileToChange because $NewFileName already exists.\" >> Daf2Mpx.errorlog");
				&ColorPrinting("off");
			}
			if(! -f $FileToChange)
			{
				&ColorPrinting("red");
				print "Counld not convert $FileToChange because file could not be found.\n";
				system("echo \"Counld not convert $FileToChange because file could not be found.\" >> Daf2Mpx.errorlog");
				&ColorPrinting("off");				
			}
		}
	}
}

sub main
{
   local (@FileListing);
   local ($NextFileToConvert);
   
	&ReadArguments();
	if(-f "Daf2Mpx.errorlog")
	{
	    unlink("Daf2Mpx.errorlog");
	}
	if(-f "Daf2Mpx.convertlog")
	{
	    unlink("Daf2Mpx.convertlog");
	}
   if((-d $SourceDir)&&(-d $DestDir))
   {
		$OurPid = $$;
		$TempFile = "ConvertTemp".$OurPid;
      system("find \"$SourceDir\"|grep -i daf\$ > \"$TempFile\"");
 
      $FilePointer = new FileHandle $TempFile, "r";
		if($FilePointer)
		{
			while(! $FilePointer->eof)
			{
				$NextFileToConvert = $FilePointer->getline;
				$NextFileToConvert =~ s/\n//g;
				
				if($NextFileToConvert gt "")
				{
					if(-f $NextFileToConvert)
					{
						&ConvertThisFile($NextFileToConvert);
					}
				}
			}  
			unlink($TempFile);			
		} else
		{
			print "Could not open file $TempFile\n";
		}
   } else
   {
      print "Error: Check your arguments for source and destination directories.\n";
      if(! -d $SourceDir)
      {
         print "I could not find the source directory.\n";
      }
      if(! -d $DestDir)
      {
         print "I could not find the destination directory.\n";
      }
   }
	system("cat Daf2Mpx.errorlog");
	print "You can read Daf2Mpx.errorlog and Daf2Mpx.convertlog\n";
}

sub ReadDafId
{
   local ($DafFile) = @_;
   local ($ReadByte);
   local ($ByteValue);
   local ($TempReturn);
   local ($ArtistName,$TrackTitle);
   local ($LastChar);
	local ($BreakOutCount);
	local ($FoundData);

   $TempReturn = "";
	if(open(INFILE,"<$DafFile"))
	{
		binmode INFILE;
	
		# Seek untill \n is found
		$Done = 0;
		$FoundData = 0;
		$BreakOutCount = 0;
		# Read to end of first DAF version informaion
		while($Done == 0)
		{
			if(read(INFILE,$ReadByte,1))
			{
				$ByteValue = ord($ReadByte);
				if(($ByteValue < 32) || ($ByteValue > 127))
				{
					$Done = 1;
					$FoundData = 1;
				}
			}				
			$BreakOutCount++;
			if($BreakOutCount > 2000)
			{
				print "Error: BreakOutCount\n";
			}
		}
		# Read to end of second DAF version informaion that may or may not exist
		$Done = 0;
		$BreakOutCount = 0;
		while($Done == 0)
		{
			if(read(INFILE,$ReadByte,1))
			{
				$ByteValue = ord($ReadByte);
				if(($ByteValue < 32) || ($ByteValue > 127))
				{
					$Done = 1;
				}
			}				
			$BreakOutCount++;
			if($BreakOutCount > 2000)
			{
				print "Error: BreakOutCount\n";
				$FoundData = 1;
			}
		}
		
		if($FoundData eq 1)
		{
			$Done = 0;
			$LastChar = "";
			$BreakOutCount = 0;
			
			# The artist and title informaion begin after the DAF version informaion and end with a \0 char but never longer then 50 chars.
			while($Done == 0)
			{
				if(read(INFILE,$ReadByte,1))
				{
					$ByteValue = ord($ReadByte);
					$BreakOutCount++;
					if(($ByteValue == 0) && (length($TempReturn) > 0))
					{
						$Done = 1;
					} 
					elsif(($ByteValue > 31)&&($ByteValue < 127))
					{
						$LastChar = $ReadByte;
						$TempReturn = $TempReturn.$ReadByte;
					}
					if($BreakOutCount > 50)
					{
						$Done = 1;
					}    
				} else
				{
					$Done = 1;
				}         
			}
			$TempReturn = &TrimText($TempReturn);
			$TempReturn =~ s/\*/_/g;
			$TempReturn =~ s/\?/_/g;
			$TempReturn =~ s/\\/_/g;
			$TempReturn =~ s/\//-/g;
			$TempReturn =~ s/&/_/g;
			$TempReturn =~ s/"/_/g;
			# " Stop syntax highlight problem in mcedit 
			$TempReturn =~ s/'/_/g;
			# ' Stop syntax highlight problem in mcedit 
		}
		close(INFILE);
	}
   return($TempReturn);
}

sub TrimText
{
   local ($PassedInText) = @_;
   local ($TempReturn);
   local ($LastChar);
   local ($StringLenth);
   
   $TempReturn = $PassedInText;
   while(index($TempReturn,"  ",0) > -1)
   {
      $TempReturn =~ s/  / /g;
   }      
   $StringLenth = length($TempReturn);
   $LastChar = substr($TempReturn,$StringLenth - 1,$StringLenth);
   if($LastChar eq " ")
   {
      $TempReturn = substr($TempReturn,0,$StringLenth - 1);
   }
   
   return($TempReturn);
}

sub ColorPrinting
{
   local ($ColorSetting) = @_;
   local ($ColorString);
   
   $ColorString = "";
   if($ColorSetting eq "red")
   {
      $ColorString = "\33[01;31m";
   }
   elsif($ColorSetting eq "green")
   {
      $ColorString = "\33[01;32m";
   }
   elsif($ColorSetting eq "yellow")
   {
      $ColorString = "\33[01;33m";
   }
   elsif($ColorSetting eq "blue")
   {
      $ColorString = "\33[01;34m";
   }
   elsif($ColorSetting eq "purple")
   {
      $ColorString = "\33[01;35m";
   }
   elsif($ColorSetting eq "teal")
   {
      $ColorString = "\33[01;36m";
   }
   elsif($ColorSetting eq "white")
   {
      $ColorString = "\33[01;37m";
   }
   elsif($ColorSetting eq "grey")
   {
      $ColorString = "\33[01;39m";
   }
   elsif($ColorSetting eq "off")
   {
      $ColorString = "\33[0m";
   }
   
   if($ColorString gt "")
   {
      print "$ColorString";
   }
}
