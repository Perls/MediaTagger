#!/usr/bin/perl -w


#=============================================================================
#
#    Copyright (C) 2019 Silicon Tao Technology Systems
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#=============================================================================

package ResizeCoverArt;

=pod

=head1 NAME
Resize images.

=cut


use Image::Size;
use Image::Magick;
use File::Copy;
use File::Find;
use File::Basename;

# To use the modules in the local directory
use FindBin qw($RealBin);
use lib "$RealBin";
use MtCommon;

%Op = ( 
	"-r" => "REPORT", # Show information about the file
	"-s" => "SCALE", # Change the image ratio by cropping
	"-d" => "DEBUG", # Show debug output
	"-v" => "VERBOSE", # Verbosly describe what is happening.
);

sub Main() {
	ReadArgs();
	ShowHelp() if $Options{HELP};
	foreach $Path (@WorkPaths) {
		if(-f $Path) { 
			EditCover($Path); 
			next;
		}
		if(-d $Path) {
			print "Finding in path = $Path\n" if $Options{DEBUG};
			find(\&wanted, $Path);
		}
	}
	PrintRecomendations();
}

sub wanted {
	/.*\.(jpg|jpeg|png)$/ && EditCover($File::Find::name);
}

sub EditCover {
	my $ImageFile = shift;
	my $Failed = 10;
	my $Orig;

	my @Cnames = ("cover.jpg", "folder.jpg", "front.jpg", "cover.png", "folder.png", "front.png");
	foreach my $Cn (@Cnames) {		
		# The return from grep must be in parenthesis to get the name of the entry, or else it just returns status code.
		if($ImageFile =~ m/$Cn$/i) {
			$Failed = 0;
			return if $ImageFile =~ m/_orig./;
			$Orig = $ImageFile;
			$Orig =~ s/(.*)\.(.*)/$1_orig.$2/;
			
			printf "Check image file '$ImageFile'\n" if $Options{DEBUG};
			my ($ImageX, $ImageY) = imgsize($ImageFile);
			printf("%s %dx%d\n", $ImageFile, $ImageX, $ImageY) if $Options{DEBUG};
			my $Diff = $ImageX - $ImageY;
			if($Diff < 0) { $Diff = $ImageY - $ImageX; }
			if($Diff > (($ImageY + $ImageX) /20) ) { 
				$Failed = 1; 
				print "CropImage '$ImageFile'\n" if $Options{DEBUG};
			}
			if(($ImageY + $ImageX) > 1000) { 
				$Failed = 1; 
				print "ScaleImage '$ImageFile'\n" if $Options{DEBUG};				
			}
			if(($ImageY + $ImageX) < 100) { 
				$Failed = 5; 
				# move($ImageFile, $Orig) if(! -f $Orig);
				print "DownloadImage replacement '$ImageFile'\n";
			}
			# print "Fail count = $Failed\n"; # Really should not use that image.
			if($Failed == 0) { 
				printf "Image file acceptable '$ImageFile'\n" if $Options{DEBUG};
				last;
			}
		}		
	}

	# We found an image with an acceptable name but it needs to be resized.
	if($Failed == 1) {
		printf "Saving oringal and resizing '$ImageFile'\n";
		move($ImageFile, $Orig) if(! -f $Orig);
		my $image = Image::Magick->new();
		$image->Read($Orig);
		$image->Resize(
			('width'  => 300),
			('height' => 300),
		);
		$image->Write(filename => $ImageFile);
	}

	# Should check if the image is the right size and shape
	# The directory may have a proper cover image file, this script is not looking for that.
	# This script only looks for image files of proper names, then tries to resize them if needed.
	# my $Idir = dirname($ImageFile);
	# Recomend("DownloadImage new image file for '$Idir'") if ($Failed == 10);
}

Main();
