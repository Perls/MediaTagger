#!/usr/bin/perl


#=============================================================================
#
#    Copyright (C) 2003-2019 Silicon Tao Technology Systems
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#=============================================================================

use FileHandle;
use File::Copy;

# Global vars
$ScottFilesDir = "";
$Mp3FilesDir = "";
$ShowHelp = 0;
$ShowSettings = 0;
$TheSettingsFile = "";
$ScottListingFile = "";
$NoArtistDefault = "";
$WriteID3Tags = "";
$ErrorLogFile = "";
$ConvertLogFile = "";
$TestedFileCount = 0;
$NewFilesConvertedCount = 0;
$ErrorCount = 0;
$OverWriteDirs = "";
$NewDires = "SCOTT";
$Updating = "";
$SimulationMode = 0;

if(&ReadSettings == 1)
{
   &ReadArguments;
   if($ShowHelp == 1)
   {
      &ShowHelpInfo;
   }
   elsif($ShowSettings == 1)
   {
      &ShowTheSettings;
   } else
   {
      &ConvertFiles;
      &ShowResults;
      if($Updating eq "No")
      {
         &WriteConf($TheSettingsFile, "UPDATING", "Yes");         
      }
   }
} else
{
   &ShowThatDefaultWasSet;
}

exit;

sub ShowResults
{
   &ColorPrinting("yellow");
   print "----------------------------------\n";
   &ColorPrinting("off");
   print "\t\tDone\n";
   
   &ColorPrinting("yellow");
   print "Files examend = ";
   &ColorPrinting("off");
   &ColorPrinting("green");
   print "$TestedFileCount\n";
   &ColorPrinting("off");
   
   &ColorPrinting("yellow");
   print "Files converted = ";
   &ColorPrinting("off");
   &ColorPrinting("green");
   print "$NewFilesConvertedCount\n";
   &ColorPrinting("off");
   
   &ColorPrinting("yellow");
   print "Errors = ";
   &ColorPrinting("off");
   &ColorPrinting("green");
   print "$ErrorCount\n\n";
   &ColorPrinting("off");
}

sub ShowThatDefaultWasSet
{
   print "Default settings where written to \n\t$TheSettingsFile\n";
   print "Please edit it to match your needed settings.\n"; 
}

sub ReadArguments
{
   local ($Argument);
   local ($Position);
   
   foreach $Argument (@ARGV)
   {
      # This will find -s --s
      $Position = index($Argument,"-s",0);
      if(($Position > -1)&&($Position < 3))
      {
         $ShowSettings = 1;
      }
      
      # This will find -h -help --h --help
      $Position = index($Argument,"-h",0);
      if(($Position > -1)&&($Position < 3))
      {
         $ShowHelp = 1;
      }
   }
}

sub ReadSettings
{
   local ($UsersHome);
   local ($SettingsRead);
   
   $SettingsRead = 0;
   $UsersHome = &GetEnvVar("HOME");
   $TheSettingsFile = "$UsersHome/.Scott2Mp3/settings.txt";
   $ScottListingFile = "$UsersHome/.Scott2Mp3/listing.txt";
   $ErrorLogFile = "$UsersHome/.Scott2Mp3/error_log.txt";
   $ConvertLogFile = "$UsersHome/.Scott2Mp3/convert_log.txt";
   if(! -d "$UsersHome/.Scott2Mp3")
   {
      mkdir("$UsersHome/.Scott2Mp3");
   }
   if(! -f "$UsersHome/.Scott2Mp3/settings.txt")
   {
      $SettingsRead = 0;
      &WriteConf($TheSettingsFile, "SCOTT DIR", "$UsersHome/mnt/ScottServer");
      &WriteConf($TheSettingsFile, "MP3 DIR", "$UsersHome/Music");
      &WriteConf($TheSettingsFile, "NO ARTIST DEFAULT", "Misc");
      &WriteConf($TheSettingsFile, "WRITE ID3 TAGS", "No");
      &WriteConf($TheSettingsFile, "OVERWRITE DIRS", "COM,JRX");
      &WriteConf($TheSettingsFile, "NEW DIRS TREE", "SCOTT");
      &WriteConf($TheSettingsFile, "UPDATING", "No");
   } else
   {
      $SettingsRead = 1;
      $ScottFilesDir = &ReadConf($TheSettingsFile, "SCOTT DIR");
      if($ScottFilesDir eq "")
      {
         $SettingsRead = 0;
         &WriteConf($TheSettingsFile, "SCOTT DIR", "$UsersHome/mnt/ScottServer");
      } else
      {
         $Updating = &ReadConf($TheSettingsFile, "UPDATING");
      }
      
      $Mp3FilesDir = &ReadConfDef($TheSettingsFile, "MP3 DIR", "$UsersHome/Music");
      $NoArtistDefault = &ReadConfDef($TheSettingsFile, "NO ARTIST DEFAULT", "Misc");
      $WriteID3Tags = &ReadConf($TheSettingsFile, "WRITE ID3 TAGS", "No");
      $OverWriteDirs = &ReadConf($TheSettingsFile, "OVERWRITE DIRS",          "COM,JRX,MO6,MO5,TU6,TU5,WE5,WE6,TH5,TH6,FR5,FR6,SA5,SA6,SU5,SU6,FRV,FRX");
      $NewDires = &ReadConf($TheSettingsFile, "NEW DIRS TREE", "SCOTT");
   }
   return($SettingsRead);
}

sub ReadConfDef
{
   local ($FileToRead,$VarToGet,$DefaultValue) = @_;
   local ($TempReturn);

   $TempReturn = &ReadConf($FileToRead,$VarToGet);
   if($TempReturn eq "")
   {
      $TempReturn = $DefaultValue;
      &WriteConf($FileToRead,$VarToGet,$DefaultValue);
   }
   return($TempReturn);
}

sub ShowTheSettings
{
   local ($ParseLine);
   local ($FilePointer);

   print "The settings are...\n";
   $FilePointer = new FileHandle $TheSettingsFile, "r";
   while(! $FilePointer->eof)
   {
      $ParseLine = $FilePointer->getline;
      $ParseLine =~ s/\n//g;
      if($ParseLine gt "")
      {
         print "$ParseLine\n";
      }
   }
   $FilePointer->close;  
}

sub WriteConf
{
   local ($FileToWrite,$VarToSet,$ValueToSet) = @_;
   local ($WriteFilePointer,$ReadFilePointer);
   local ($TempFileName);
   local ($FoundInConf);
   local ($ParseLine);
   local ($ReadVar,$ReadVal);
   local ($PostLine);

   if(! -f $FileToWrite)
   {
      $WriteFilePointer = new FileHandle $FileToWrite, "w";
      print $WriteFilePointer "\n";
      $WriteFilePointer->close;
   }
   
   $TempFileName = $FileToWrite."_tmp";
   move($FileToWrite,$TempFileName);
   
   $FoundInConf = 0;
   $ReadFilePointer = new FileHandle $TempFileName, "r";
   $WriteFilePointer = new FileHandle $FileToWrite, "w";
   while(! $ReadFilePointer->eof)
   {
      $ParseLine = $ReadFilePointer->getline;
      $ParseLine =~ s/\n//g;
      if($ParseLine gt "")
      {
         ($ReadVar,$ReadVal) = split/ = /,$ParseLine;
         if($ReadVar eq $VarToSet)
         {
            $FoundInConf = 1;
            $PostLine = $VarToSet." = ".$ValueToSet."\n";
            print $WriteFilePointer $PostLine;
         } else
         {
            $PostLine = $ParseLine."\n";
            if($ParseLine gt "")
            {
               print $WriteFilePointer $PostLine;
            }
         }
      }
   }
   $ReadFilePointer->close;  
   if($FoundInConf == 0)
   {
      $PostLine = $VarToSet." = ".$ValueToSet."\n";
      print $WriteFilePointer $PostLine;
   }   
   $WriteFilePointer->close;
   
   if(-f $TempFileName)
   {
      unlink($TempFileName);
   }
}

sub ReadConf
{
   local ($FileToRead,$VarToGet) = @_;
   local ($FilePointer);
   local ($ReadVar,$ReadVal);
   local ($DoneLooking);
   local ($TempReturn);

   $TempReturn = "";
   $DoneLooking = 0;
   $FilePointer = new FileHandle $FileToRead, "r";
   while($DoneLooking == 0)
   {
      $ParseLine = $FilePointer->getline;
      $ParseLine =~ s/\n//g;
      if($ParseLine gt "")
      {
         ($ReadVar,$ReadVal) = split/ = /,$ParseLine;
         if($ReadVar eq $VarToGet)
         {
            $DoneLooking = 1;      
            $TempReturn = $ReadVal;
         }
      }
      if($FilePointer->eof)
      {
         $DoneLooking = 1;
      }
   }
   $FilePointer->close;  
   return($TempReturn);
}

sub GetEnvVar
{
   local ($VarName) = @_;
   local ($TempReturn);
   
   #print "Posted = ".$ENV{'QUERY_STRING'};
   foreach $key (keys %ENV)
   {
      if($key eq $VarName)
      {
         $TempReturn = $ENV{$key};
      }
      #print "$key = $ENV{$key}\n";
   }
   return($TempReturn);
}

sub ShowHelpInfo
{
   print "Scott2Mp3.pl from www.SiliconTao.com\tGNU GPL licensed!\n\n";
   print "This program converts Scott files into MP3 format. Scott files are music\n";
   print "files in MP3 format with invalid ID tags and incorect file extentions.\n\n";
   print "This program will attempt to copy them from your \"SCOTT DIR\" to your\n";
   print "\"MP3 DIR\" and then alter them there.\n";
   print "Your two settings for where these directories are must be set in your\n";
   print "settings file in your home directory. For you this file is located at...\n";
   print "\t$TheSettingsFile\n";
   print "Please make sure this file contains the correct information then run this\n";
   print "program again.\n\n";
   print "To view the contents of this file use -s\n";
}

sub ConvertThisFile
{
   local ($FileToChange) = @_;
   local ($NewFileName);
   local ($AuthorName);
   local ($TheScottInfo);
   local ($NewSavedFile);
   local ($ConvertWorked);
   local ($ScottDirName);
   local ($StringPosition);
   local ($TargetDirectory);
   local ($OverWriteFiles);
   local ($ScottPrefix);
   
   $OverWriteFiles = 0;
   $ConvertWorked = 0;
   $NewSavedFile = "";
   $NewFileName = "";
   $TheScottInfo = &ReadScottId($FileToChange);
   if($TheScottInfo gt "")
   {
      $NewFileName = $TheScottInfo.".mp3";
      ($AuthorName,$TrackTitle) = split/ - /,$NewFileName;
      if(($AuthorName eq "")||($TrackTitle eq ""))
      {
         $AuthorName = $NoArtistDefault;
         $NewFileName = $NoArtistDefault." - ".$NewFileName;
      }
      if($NewDires eq "SCOTT")
      {
         $ScottDirName = $FileToChange;
         $StringPosition = rindex($ScottDirName,"/",length($ScottDirName));
         $ScottDirName = substr($ScottDirName,0,$StringPosition);
         $StringPosition = rindex($ScottDirName,"/",length($ScottDirName));
         $ScottDirName = substr($ScottDirName,$StringPosition + 1,length($ScottDirName));
         $TargetDirectory = "$Mp3FilesDir/$ScottDirName";
         $OverWriteFiles = &DetectIfOverWriteDir($ScottDirName);
         if($OverWriteFiles == 1)
         {
            $ScottPrefix = $FileToChange;
            $StringPosition = rindex($ScottPrefix,"/",length($ScottPrefix));
            $ScottPrefix = substr($ScottPrefix,$StringPosition,length($ScottPrefix));
            $ScottPrefix = substr($ScottPrefix,0,length($ScottPrefix)-4);
            $NewFileName = $ScottPrefix."_".$NewFileName;
         }
      } else
      {
         $TargetDirectory = "$Mp3FilesDir/$AuthorName";
      }
      if(! -d $TargetDirectory)
      {
         mkdir($TargetDirectory);
      }      
      $NewSavedFile = "$TargetDirectory/$NewFileName";
      $ConvertWorked = 1;
      if((! -f $NewSavedFile)||($OverWriteFiles == 1))
      {
         $ConvertWorked = 0;
         copy($FileToChange,$NewSavedFile);
         if($WriteID3Tags eq "Yes")
         {
            if(&SaveId3Tag($NewSavedFile,$TheScottInfo) == 1)
            {
               $ConvertWorked = 1;
            }
         } else
         {
            $ConvertWorked = 1;
         }
         if($ConvertWorked == 1)
         {
            $NewFilesConvertedCount++;
            &ConvertLog("Converted $FileToChange to $NewFileName");
            &ColorPrinting("green");
            if($Updating eq "Yes")
            {
               &UpdateAddsDir($NewSavedFile);
            }
            print "Success in converting $NewFileName\n";
         } else
         {
            $ErrorCount++;
            &ColorPrinting("red");
            print "Error in converting a file, read the log $ErrorLogFile\n";
            &LogError("Error in converting from $FileToChange to $NewFileName");
         }
         &ColorPrinting("off");
      }
   }
}

sub DetectIfOverWriteDir
{
   local ($DirToTest) = @_;
   local ($TempReturn);
   local (@DirList);
   
   $DirToTest = uc($DirToTest);
   @DirList = split/,/,$OverWriteDirs;
   $TempReturn = 0;
   foreach $DirInList (@DirList)
   {
      $DirInList = uc($DirInList);
      if($DirInList eq $DirToTest)
      {
         $TempReturn = 1;
      }      
   }   
   return($TempReturn);
}

sub UpdateAddsDir
{
   local ($SavedFile) = @_;
   local ($ScottDir);
   local ($ScottDirName);
   local ($StringPosition);
   local ($TargetDirectory);
   local ($OverWriteFiles);
   local ($BranchID);
   local ($AddsDir);
   
   $ScottDirName = $SavedFile;
   $StringPosition = rindex($ScottDirName,"/",length($ScottDirName));
   $ScottDirName = substr($ScottDirName,0,$StringPosition);
   $StringPosition = rindex($ScottDirName,"/",length($ScottDirName));
   $ScottDirName = substr($ScottDirName,$StringPosition + 1,length($ScottDirName));
   $TargetDirectory = "$Mp3FilesDir/$ScottDirName";
   $OverWriteFiles = &DetectIfOverWriteDir($ScottDirName);
   $BranchID = substr($ScottDirName,0,1);
   if(($BranchID eq "1")||($BranchID eq "2"))
   {
      $AddsDir = "$Mp3FilesDir/$BranchID"."00ADDS";
      
      if(! -d $AddsDir)
      {
         mkdir($AddsDir);
      }
      copy($SavedFile,$AddsDir);
   }
}

sub LogError
{
   local ($LogText) = @_;
   local ($FilePointer);

   $FilePointer = new FileHandle "$ErrorLogFile", "a";
   print $FilePointer $LogText."\n";
   $FilePointer->close;
}

sub ConvertLog
{
   local ($LogText) = @_;
   local ($FilePointer);

   $FilePointer = new FileHandle "$ConvertLogFile", "a";
   print $FilePointer $LogText."\n";
   $FilePointer->close;
}

sub ConvertFiles
{
   local (@FileListing);
   local ($NextFileToConvert);
   
   if((-d $ScottFilesDir)&&(-d $Mp3FilesDir))
   {
      system("find \"$ScottFilesDir\"|grep -i wav\$ > \"$ScottListingFile\"");
 
      $FilePointer = new FileHandle $ScottListingFile, "r";
      while(! $FilePointer->eof)
      {
         $NextFileToConvert = $FilePointer->getline;
         $NextFileToConvert =~ s/\n//g;
         if($NextFileToConvert gt "")
         {
            if(-f $NextFileToConvert)
            {
               $TestedFileCount++;
               &ConvertThisFile($NextFileToConvert);
            }
         }
      }   
   } else
   {
      print "Check your settings file for errors.\n";
      print "\t$TheSettingsFile\n";
      if(! -d $ScottFilesDir)
      {
         print "I could not find the directory for and of...\n";
         print "\tSCOTT DIR = $ScottFilesDir\n\n";
      }
      if(! -d $Mp3FilesDir)
      {
         print "I could not find the directory for and of...\n";
         print "\tMP3 DIR = $Mp3FilesDir\n\n";
      }
   }
}

sub ReadScottId
{
   local ($ScottFile) = @_;
   local ($ReadByte);
   local ($ByteValue);
   local ($TempReturn);
   local ($ArtistName,$TrackTitle);
   local ($LastChar);

   $ArtistName = "";
   $TrackTitle = "";
   $TempReturn = "";
   if($SimulationMode == 1)
   {
      $ArtistName = &GetRandomText(10,6,1,1,0);
      $TrackTitle = &GetRandomText(10,6,1,1,0);
      $TempReturn = $ArtistName." - ".$TrackTitle;
   } else
   {
      if(open(INFILE,"<$ScottFile"))
      {
         binmode INFILE;
      
         # Title of track starts at byte number 0x48 terminated by more then one space
         seek(INFILE,0x48,0);
         $Done = 0;
         $LastChar = "";
         while($Done == 0)
         {
            if(read(INFILE,$ReadByte,1))
            {
               $ByteValue = ord($ReadByte);
               if(($LastChar eq " ")&&($ReadByte eq " "))
               {
                  $Done = 1;
               } 
               elsif(($ByteValue > 31)&&($ByteValue < 127))
               {
                  $LastChar = $ReadByte;
                  $TrackTitle = $TrackTitle.$ReadByte;
               } else
               {
                  $Done = 1;
               }    
            } else
            {
               $Done = 1;
            }         
         }
         $TrackTitle = &TrimText($TrackTitle);

         # Name of artist starts at byte number 0x014F terminated by more then one space
         seek(INFILE,0x14F,0);
         $Done = 0;
         $LastChar = "";
         while($Done == 0)
         {
            if(read(INFILE,$ReadByte,1))
            {
               $ByteValue = ord($ReadByte);
               if(($LastChar eq " ")&&($ReadByte eq " "))
               {
                  $Done = 1;
               } 
               elsif(($ByteValue > 31)&&($ByteValue < 127))
               {
                  $LastChar = $ReadByte;
                  $ArtistName = $ArtistName.$ReadByte;
               } else
               {
                  $Done = 1;
               }            
            } else
            {
               $Done = 1;
            }         
         }
         $ArtistName = &TrimText($ArtistName);
      
         if(($ArtistName gt "")&&($TrackTitle gt ""))
         {
            $TempReturn = $ArtistName." - ".$TrackTitle;
         } else
         {
            $TempReturn = $ArtistName.$TrackTitle;
         }
      }
      $TempReturn =~ s/\*/_/g;
      $TempReturn =~ s/\?/_/g;
      $TempReturn =~ s/\\/_/g;
      $TempReturn =~ s/\//_/g;
      $TempReturn =~ s/&/_/g;
      $TempReturn =~ s/"/_/g;
      $TempReturn =~ s/'/_/g;
   }
   return($TempReturn);
}

sub GetRandomText
{
	local ($MaxLength,$MinLength,$UpperCaseLetters,$LowerCaseLetters,$Numbers) = @_;
   local ($TempReturn);
   local ($ALoopCounter);
   local ($NewRandomNumber);
   local ($MaxLoop);
   local ($NextChar);
	local ($LengthChange);

	$LengthChange = $MaxLength - $MinLength;
   $TempReturn = "";
	$MaxLoop = 0;
   if($LengthChange > 0)
	{
		$MaxLoop = int(rand(9));
	}
   $MaxLoop = $MaxLoop + $MinLength;
	$ALoopCounter = 0;
	if(($Letters == 1)||($Numbers == 1))
	{
		while($ALoopCounter < $MaxLoop)
		{
      	$NextChar = int(rand(61)) + 1;
      	if($NextChar < 27)
      	{ 
				# Add a lower case letter
				if($LowerCaseLetters == 1)
				{
					$ALoopCounter++;
         		$NextChar = 96 + $NextChar;
         		$TempReturn = $TempReturn . pack('C',$NextChar);
				}
      	}
      	elsif($NextChar < 27+26)
      	{
				# Add an upper case letter
				if($UpperCaseLetters == 1)
				{
					$ALoopCounter++;
         		$NextChar = 64 - 26 + $NextChar;
         		$TempReturn = $TempReturn . pack('C',$NextChar);
				}
      	} else
      	{
				# Add a number 
				if($Numbers == 1)
				{
					$ALoopCounter++;
         		$NextChar = 48 - (27 + 26) + $NextChar;
         		$TempReturn = $TempReturn . pack('C',$NextChar);
				}
      	}
   	}
	}
   return($TempReturn);
}

sub SaveId3Tag
{


}

sub TrimText
{
   local ($PassedInText) = @_;
   local ($TempReturn);
   local ($LastChar);
   local ($StringLenth);
   
   $TempReturn = $PassedInText;
   while(index($TempReturn,"  ",0) > -1)
   {
      $TempReturn =~ s/  / /g;
   }      
   $StringLenth = length($TempReturn);
   $LastChar = substr($TempReturn,$StringLenth - 1,$StringLenth);
   if($LastChar eq " ")
   {
      $TempReturn = substr($TempReturn,0,$StringLenth - 1);
   }
   
   return($TempReturn);
}

sub ColorPrinting
{
   local ($ColorSetting) = @_;
   local ($ColorString);
   
   $ColorString = "";
   if($ColorSetting eq "red")
   {
      $ColorString = "\33[01;31m";
   }
   elsif($ColorSetting eq "green")
   {
      $ColorString = "\33[01;32m";
   }
   elsif($ColorSetting eq "yellow")
   {
      $ColorString = "\33[01;33m";
   }
   elsif($ColorSetting eq "blue")
   {
      $ColorString = "\33[01;34m";
   }
   elsif($ColorSetting eq "purple")
   {
      $ColorString = "\33[01;35m";
   }
   elsif($ColorSetting eq "teal")
   {
      $ColorString = "\33[01;36m";
   }
   elsif($ColorSetting eq "white")
   {
      $ColorString = "\33[01;37m";
   }
   elsif($ColorSetting eq "grey")
   {
      $ColorString = "\33[01;39m";
   }
   elsif($ColorSetting eq "off")
   {
      $ColorString = "\33[0m";
   }
   
   if($ColorString gt "")
   {
      print "$ColorString";
   }
}

