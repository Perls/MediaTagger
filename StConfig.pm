#!/usr/bin/perl -w

package StConfig;

use File::Basename;
use File::HomeDir;

sub ReadConfig {
	my $AppVar = shift;
	my $AppName = basename($0);
	$AppName =~ s/.pl$/.conf/;
	my $ConfFile = File::HomeDir->my_home."/.".$AppName;
	if(! -f $ConfFile) {
		printf STDERR "Please create $ConfFile\n";
		exit 2;
	}

	open IN, $ConfFile;
	while (my $Line = <IN>) {
		chomp $Line;
		my ($Var, $Val) = split /=/, $Line;
		$AppVar->{$Var} = $Val;
	}
	close IN;
}

1;